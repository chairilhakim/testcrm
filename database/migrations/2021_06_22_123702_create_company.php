<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompany extends Migration {

    public function up() {
        Schema::create('company', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 60);
            $table->string('email', 100);
            $table->string('logo', 120)->nullable();
            $table->string('website', 100);
            $table->string('code', 30);
            $table->tinyInteger('is_active')->default(1)->comment('0 = tidak aktif, 1 = aktif');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            
            $table->index('name');
            $table->index('email');
            $table->index('website');
            $table->index('code');
            $table->index('is_active');
            $table->index('created_at');
            $table->index('deleted_at');
        });
    }

    public function down() {
        Schema::dropIfExists('company');
    }
}
