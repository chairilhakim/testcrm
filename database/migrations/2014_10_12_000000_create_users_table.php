<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {

    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('f_name', 60);
            $table->string('l_name', 100);
            $table->string('email', 100);
            $table->string('phone', 25)->nullable();
             $table->string('user_code', 30)->nullable();
            $table->string('password', 100);
            $table->tinyInteger('is_active')->default(1)->comment('0 = tidak aktif, 1 = aktif');
            $table->smallInteger('user_type')->default(10)->comment('1 = super admin, 5 = admin company, 10 = employee');
            $table->integer('company_id')->default(0);
            
            $table->tinyInteger('is_profile')->default(0)->comment('0 = belum, 1 = sudah');
            $table->smallInteger('gender')->nullable()->comment('1 = laki-laki, 2 = perempuan');
            $table->string('alamat', 255)->nullable();
            $table->string('provinsi', 70)->nullable();
            $table->string('kota', 100)->nullable();
            $table->string('kecamatan', 120)->nullable();
            $table->string('kelurahan', 120)->nullable();
            
            $table->timestamp('active_at')->nullable();
            $table->timestamp('profile_created_at')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->rememberToken();
            
            $table->index('f_name');
            $table->index('l_name');
            $table->index('email');
            $table->index('password');
            $table->index('phone');
            $table->index('user_code');
            $table->index('is_active');
            $table->index('user_type');
            $table->index('company_id');
            
            $table->index('is_profile');
            $table->index('gender');
            $table->index('provinsi');
            $table->index('kota');
            $table->index('kecamatan');
            $table->index('kelurahan');
            
            $table->index('active_at');
            $table->index('profile_created_at');
            $table->index('created_at');
            $table->index('deleted_at');
        });
    }

    public function down() {
        Schema::dropIfExists('users');
    }
}
