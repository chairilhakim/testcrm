<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class DatabaseSeeder extends Seeder {
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        
        DB::table('users')->delete();
        $users = array(
            array(
                'f_name' => 'Super',
                'l_name' => 'Admin',
                'email' => 'superadmin@admin.us',
                'password' => bcrypt('superadmin_2021'),
                'user_code' => 'superadmin@admin',
                'user_type' => 1,
            ),
        );
        foreach($users as $row){
            DB::table('users')->insert($row);
        }
        
        dd('Done Seed');
    }
}
