<!DOCTYPE html>
<html lang="en">
    <head>
        <title>CRM</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta http-equiv="X-UA-Compatible" content="IE=8, IE=9, IE=10, IE=edge, chrome=1" />
        <meta http-equiv="CHARSET" content="UTF-8" />
        <meta http-equiv="VW96.OBJECT TYPE" content="Document" />
        <meta http-equiv="x-dns-prefetch-control" content="off" />
        <meta name="description" content="CRM" />
        <meta name="keywords" content="CRM">
        <meta name="author" content="CRM" />
        <link rel="icon" href="/favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    </head>
    <div class="auth-wrapper">
        <div class="auth-content">
            <div class="card">
                <div class="row align-items-center text-center">
                    <div class="col-md-12">
                        <form method="post" action="/login_admin">
                            {{ csrf_field() }}
                            <div class="card-body">
                                <h4 class="mb-3 f-w-400">Member Sign In</h4>
                                @if ( Session::has('message') )
                                    <div class="widget-content mt10 mb10 mr15">
                                        <div class="alert alert-{{ Session::get('messageclass') }}">
                                            <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                            {{  Session::get('message')    }} 
                                        </div>
                                    </div>
                                @endif
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="feather icon-user"></i></span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Username" name="admin_username">
                                </div>
                                <div class="input-group mb-4">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="feather icon-lock"></i></span>
                                    </div>
                                    <input type="password" class="form-control" placeholder="Password" name="admin_password">
                                </div>
                                <button class="btn btn-block btn-primary mb-4">Log In</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('assets/js/vendor-all.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/waves.min.js') }}"></script>
    </body>
</html>