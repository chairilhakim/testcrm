<!DOCTYPE html>
<html lang="en">
    <head>
        <title>CRM</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="description" content="CRM">
        <meta name="author" content="CRM">
        <meta name="keywords" content="CRM">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
        @yield('styles')
    </head>

    <body class="">

        @yield('content')

    <script src="{{ asset('assets/js/vendor-all.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/pcoded.min.js') }}"></script>
    @yield('javascript')
    </body>
</html>