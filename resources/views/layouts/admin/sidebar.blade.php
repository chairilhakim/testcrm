<nav class="pcoded-navbar menupos-fixed menu-light ">
    <div class="navbar-wrapper  ">
        <div class="navbar-content scroll-div " >
            <ul class="nav pcoded-inner-navbar ">
                <li class="nav-item pcoded-menu-caption">
                    <label>Menu</label>
                </li>
                <li class="nav-item">
                    <a href="{{ URL::to('/') }}/dashboard" class="nav-link ">
                        <span class="pcoded-micon">
                            <i class="feather icon-home"></i>
                        </span>
                        <span class="pcoded-mtext">
                            Dashboard
                        </span>
                    </a>
                </li>
                
                @if($dataUser->user_type == 1)
                <li class="nav-item pcoded-hasmenu">
                    <a href="#!" class="nav-link ">
                        <span class="pcoded-micon"><i class="feather icon-users"></i></span>
                        <span class="pcoded-mtext">
                            Company
                        </span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li><a href="{{ URL::to('/') }}/add/company">New</a></li>
                        <li><a href="{{ URL::to('/') }}/list/company">List</a></li>
                        <li><a href="{{ URL::to('/') }}/list/admin/company">Admin Company</a></li>
                    </ul>
                </li>
                @endif
                
                @if($dataUser->user_type == 5)
                <li class="nav-item pcoded-hasmenu">
                    <a href="#!" class="nav-link ">
                        <span class="pcoded-micon"><i class="feather icon-users"></i></span>
                        <span class="pcoded-mtext">
                            Employee
                        </span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li><a href="{{ URL::to('/') }}/add/employee">New</a></li>
                        <li><a href="{{ URL::to('/') }}/list/employee">List</a></li>
                    </ul>
                </li>
                @endif
                
                @if($dataUser->user_type == 10)
                
                @endif
                
                <li class="nav-item">
                    <a href="{{ URL::to('/') }}/logout" class="nav-link "><span class="pcoded-micon"><i class="feather icon-log-out text-c-red"></i></span><span class="pcoded-mtext">Logout</span></a>
                </li>
            </ul>
        </div>
    </div>
</nav>