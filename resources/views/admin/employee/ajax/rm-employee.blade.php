<form class="login100-form validate-form" method="post" action="/rm/employee">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
    </div>
    <div class="modal-body">
        {{ csrf_field() }}
        @if($getData != null)
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <h4 class="text-danger" style="text-align: center;">Do you want to remove this employee?</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" value="{{$getData->f_name}} {{$getData->l_name}}" required="">
                    </div>
                </div>
            </div>
            <input type="hidden" name="cekId" value="{{$getData->id}}" >
        @else 
            No Data
        @endif
    </div>
    
    <div class="modal-footer">
        <div class="left-side">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        <div class="divider"></div>
        @if($getData != null)
        <div class="right-side">
            <button type="submit" class="btn btn-info">Confirm</button>
        </div>
        @endif
    </div>
</form>   