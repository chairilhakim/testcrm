<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>CRM | Welcome</title>
        <style type="text/css">*{-webkit-font-smoothing:antialiased}body{Margin:0;padding:0;min-width:100%;font-family:Arial,sans-serif;-webkit-font-smoothing:antialiased;mso-line-height-rule:exactly}table{border-spacing:0;color:#333;font-family:Arial,sans-serif}img{border:0}.wrapper{width:100%;table-layout:fixed;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}.webkit{max-width:800px}.outer{Margin:0 auto;width:100%;max-width:800px}.full-width-image img{width:100%;max-width:800px;height:auto}.inner{padding:10px}p{Margin:0;padding-bottom:10px}.h1{font-size:21px;font-weight:bold;Margin-top:15px;Margin-bottom:5px;font-family:Arial,sans-serif;-webkit-font-smoothing:antialiased}.h2{font-size:18px;font-weight:bold;Margin-top:10px;Margin-bottom:5px;font-family:Arial,sans-serif;-webkit-font-smoothing:antialiased}.one-column .contents{text-align:left;font-family:Arial,sans-serif;-webkit-font-smoothing:antialiased}.one-column p{font-size:14px;Margin-bottom:10px;font-family:Arial,sans-serif;-webkit-font-smoothing:antialiased}.two-column{text-align:center;font-size:0}.two-column .column{width:100%;max-width:300px;display:inline-block;vertical-align:top}.contents{width:100%}.two-column .contents{font-size:14px;text-align:left}.two-column img{width:100%;max-width:280px;height:auto}.two-column .text{padding-top:10px}.three-column{text-align:center;font-size:0;padding-top:10px;padding-bottom:10px}.three-column .column{width:100%;max-width:200px;display:inline-block;vertical-align:top}.three-column .contents{font-size:14px;text-align:center}.three-column img{width:100%;max-width:180px;height:auto}.three-column .text{padding-top:10px}.img-align-vertical img{display:inline-block;vertical-align:middle}@media only screen and (max-device-width: 480px){table[class=hide],img[class=hide],td[class=hide]{display:none !important}.contents1{width:100%}.contents1{width:100%}</style>
    </head>
    <body style="Margin:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;min-width:100%;background-color:#f3f2f0;">
        <center class="wrapper" style="width:100%;table-layout:fixed;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#f3f2f0;">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#f3f2f0;" bgcolor="#f3f2f0;">
                <tr>
                    <td width="100%">
                        <div class="webkit" style="max-width:800px;Margin:0 auto;">
                            <table class="outer" align="center" cellpadding="0" cellspacing="0" border="0" style="border-spacing:0;Margin:0 auto;width:100%;max-width:800px;">
                                <tr>
                                    <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                        <table border="0" width="100%" cellpadding="0" cellspacing="0" >
                                            <tr>
                                                <td>
                                                    <table style="width:100%;" cellpadding="0" cellspacing="0" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center">
                                                                    <center>
                                                                        <table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" style="Margin: 0 auto;">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="one-column" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" bgcolor="#FFFFFF">
                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#f3f2f0">
                                                                                            <tr>
                                                                                                <td class="two-column" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;text-align:center;font-size:0;" >
                                                                                                    <div class="column" style="width:100%;max-width:220px;display:inline-block;vertical-align:top;">
                                                                                                        <table class="contents" style="border-spacing:0; width:100%">
                                                                                                            <tr>
                                                                                                                <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" align="middle"><a href="#" target="_blank"><img src="https://www.designmantic.com/logo-images/6803.png?company=Company+Name&slogan=&verify=1" alt="" style="border-width:0;margin-top:20px;display:block;width:200px;" align="left"/></a></td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </center>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="one-column" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-spacing:0; border-left:1px solid #e8e7e5; border-right:1px solid #e8e7e5; border-bottom:1px solid #e8e7e5" bgcolor="#FFFFFF">
                                            <tr>
                                                <td align="center" style="padding:40px 40px 60px 40px">
                                                    <p style="color:#262626; font-size:22px; text-align:center; font-family: Verdana, Geneva, sans-serif">Hello, {{$name}}</p>
                                                    <p style="color:#000000; font-size:14px; text-align:center; font-family: Verdana, Geneva, sans-serif; line-height:22px">Welcome. </p>
                                                    
                                                    ---------------------------------------------------------------------------------------------
                                                    <p style="color:#000000; margin-top:28px;font-size:16px;text-align:center; font-family: Verdana, Geneva, sans-serif; line-height:22px">Success Always!</p>
                                                    <a href="#" target="_blank" style="width:250; display:block; text-decoration:none; border:0; text-align:center; font-weight:bold;font-size:18px; font-family: Arial, sans-serif;" class="button_link">CRM</a>
                                                    
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>