@extends('layouts.admin.main')
@section('content')

<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layouts.admin.sidebar')
@include('layouts.admin.header')
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                        <h5>Assign Admin Company</h5>
                </div>
                <div class="card-body">
                    @if ( Session::has('message') )
                        <div class="alert alert-{{ Session::get('messageclass') }} alert-dismissible fade show" role="alert">
                            {{  Session::get('message')    }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                    @endif
                    <form class="needs-validation"  id="uploadVideo" action="{{ url('/') }}/edit/employee" method="post" >
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="f_name">First Name</label>
                                <input type="text" class="form-control" id="f_name" name="f_name" placeholder="First Name" required="" value="{{$getData->f_name}}">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="l_name">Last Name</label>
                                <input type="text" class="form-control" id="l_name" name="l_name" placeholder="Last Name" required="" value="{{$getData->l_name}}">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="email">Email</label>
                                <input type="text" class="form-control" id="email" name="email" placeholder="Email" required="" value="{{$getData->email}}">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="phone">Phone</label>
                                <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" required="" value="{{$getData->phone}}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="Username">Username</label>
                                <input type="text" class="form-control" id="Username" name="user_code" placeholder="Username" required="" value="{{$getData->user_code}}">
                            </div>
                            <input type="hidden" name="id" value="{{$getData->id}}">
                            <div class="form-group col-md-6">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                <div class="invalid-feedback" style="display: inherit;color: #444;">Empty for not change</div>
                            </div>
                        </div>
                        

                        <button class="btn  btn-primary" type="submit">Submit form</button>
                    </form>
                </div>
                
            </div>
        </div>
    </div>
</div>
@stop



@section('javascript')

@stop

