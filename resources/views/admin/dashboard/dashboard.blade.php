@extends('layouts.admin.main')
@section('content')

<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layouts.admin.sidebar')
@include('layouts.admin.header')
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="row">
            @if($dataUser->user_type == 1)
            <div class="col-md-6 col-xl-6">
                <div class="card bg-c-blue order-card">
                    <div class="card-body">
                        <h6 class="text-white">Total Company</h6>
                        <h2 class="text-right text-white"><i class="feather icon-award float-left"></i><span>{{$totalCompany}}</span></h2>
                   </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-6">
                <div class="card bg-c-green order-card">
                    <div class="card-body">
                        <h6 class="text-white">Total Employee</h6>
                        <h2 class="text-right text-white"><i class="feather icon-user-check float-left"></i><span>{{$totalEmployee}}</span></h2>
                   </div>
                </div>
            </div>
            @endif
            
            @if($dataUser->user_type == 5)
            <div class="col-md-6 col-xl-6">
                <div class="card bg-c-green order-card">
                    <div class="card-body">
                        <h6 class="text-white">Total Employee {{$detailCompany->name}}</h6>
                        <h2 class="text-right text-white"><i class="feather icon-user-check float-left"></i><span>{{$totalEmployee}}</span></h2>
                   </div>
                </div>
            </div>
            @endif
            
            @if($dataUser->user_type == 10)
            <div class="col-md-12 col-xl-12">
                    <h5>Welcome To {{$detailCompany->name}} CRM</h5>
                    <hr>
                    <div class="card">
                        
                        <div class="card-body">
                            <h6 class="card-title">About {{$detailCompany->name}}</h6>
                            <p class="card-text">
                                is simply dummy text of the printing and typesetting industry. 
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and 
                                scrambled it to make a type specimen book. It has survived not only five centuries, 
                                but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of 
                                Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like 
                                Aldus PageMaker including versions of Lorem Ipsum.
                            </p>
                        </div>
                    </div>
            </div>
            @endif

        </div>
    </div>
</div>
@stop
