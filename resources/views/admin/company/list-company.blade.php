@extends('layouts.admin.main')
@section('content')

<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layouts.admin.sidebar')
@include('layouts.admin.header')

<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5>List Company</h5>
                    </div>
                    <div class="card-body table-border-style">
                        @if ( Session::has('message') )
                            <div class="alert alert-{{ Session::get('messageclass') }} alert-dismissible fade show" role="alert">
                                {{  Session::get('message')    }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            </div>
                        @endif
                        <div class="table-responsive roles-table">
                            <table class="table table-striped nowrap" id="myTable">
                                <thead class="text-primary">
                                    <tr>
                                        <th>No.</th>
                                        <th>Logo</th>
                                        <th>Name</th>
                                        <!--<th>Code</th>-->
                                        <th>Email</th>
                                        <th>Website</th>
                                        <th>Join Date</th>
                                        <th>Action</th>
                                     </tr>
                                </thead>
                                <tbody>
                                    @if($getData != null)
                                    <?php $no = 0; ?>
                                        @foreach($getData as $row)
                                        <?php
                                            $no++;
                                        ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td><img src="/storage/{{$row->logo}}" style="width: 90px;"></td>
                                                <td>{{$row->name}}</td>
                                                <!--<td>{{$row->code}}</td>-->
                                                <td>{{$row->email}}</td>
                                                <td>{{$row->website}}</td>
                                                <td>{{date('d M Y H:i', strtotime($row->created_at))}}</td>
                                                <td class="td-actions text-left" >
                                                    <div class="table-icons">
                                                        <a  href="{{ URL::to('/') }}/edit/company/{{$row->id}}" class="text-primary">edit</a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/css/jquery.dataTables.min.css') }}">
@stop

@section('javascript')
<script type="text/javascript" language="javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        var myTableRow =  $('#myTable').DataTable( {
                columnDefs: [{
                    orderable: false,
                    className: 'select-checkbox',
                    targets: 0
                }],
                dom: 'Bfrtip',
                "deferRender": true,
                columnDefs: [{ 
                    orderable: false, 
                    targets: 0,
                }],
                searching: true,
                 pagingType: "full_numbers",
                 "paging":   true,
                 "info":     false,
                 "ordering": true,
                 "pageLength": 20
        } );
        
    } );
</script>
@stop
