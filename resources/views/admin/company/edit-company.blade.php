@extends('layouts.admin.main')
@section('content')

<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layouts.admin.sidebar')
@include('layouts.admin.header')
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                        <h5>Edit Company</h5>
                </div>
                <div class="card-body">
                    @if ( Session::has('message') )
                        <div class="alert alert-{{ Session::get('messageclass') }} alert-dismissible fade show" role="alert">
                            {{  Session::get('message')    }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                    @endif
                    <form class="needs-validation"  id="uploadVideo" action="{{ url('/') }}/edit/company" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Company Name" value="{{$getData->name}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="email">Email</label>
                                <input type="text" class="form-control" id="email" name="email" placeholder="Company Email" value="{{$getData->email}}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="code">Code</label>
                                <input type="text" class="form-control" id="code" name="code" placeholder="Company Code" value="{{$getData->code}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="website">Website</label>
                                <input type="text" class="form-control" id="website" name="website" placeholder="Company Website" value="{{$getData->website}}">
                            </div>
                        </div>
                        <input type="hidden" name="id" value="{{$getData->id}}">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="inputGroupFile01" name="file_upload">
                                    <label class="custom-file-label" for="inputGroupFile01">Upload Logo</label>
                                    <div class="invalid-feedback" style="display: inherit;color: #444;">Empty for not change</div>
                                </div>
                            </div>
                        </div>

                        <button class="btn  btn-primary" type="submit">Submit form</button>
                    </form>
                </div>
                
            </div>
        </div>
    </div>
</div>
@stop



@section('javascript')

@stop

