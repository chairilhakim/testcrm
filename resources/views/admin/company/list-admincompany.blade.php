@extends('layouts.admin.main')
@section('content')

<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layouts.admin.sidebar')
@include('layouts.admin.header')

<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Admin Company</h5>
                        <span class="d-block m-t-5"><a href="{{ URL::to('/') }}/assign/admin/company" type="button" class="btn  btn-primary btn-sm">Assign Admin</a></span>
                        
                    </div>
                    <div class="card-body table-border-style">
                        @if ( Session::has('message') )
                            <div class="alert alert-{{ Session::get('messageclass') }} alert-dismissible fade show" role="alert">
                                {{  Session::get('message')    }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            </div>
                        @endif
                        <div class="table-responsive roles-table">
                            <table class="table table-striped nowrap" id="myTable">
                                <thead class="text-primary">
                                    <tr>
                                        <th>No.</th>
                                        <th>Company</th>
                                        <th>Admin Name</th>
                                        <th>Action</th>
                                     </tr>
                                </thead>
                                <tbody>
                                    @if($getData != null)
                                    <?php $no = 0; ?>
                                        @foreach($getData as $row)
                                        <?php
                                            $no++;
                                        ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->name}}</td>
                                                <td>{{$row->f_name}} {{$row->l_name}}</td>
                                                <td class="td-actions text-left" >
                                                    <div class="table-icons">
                                                        <a  href="{{ URL::to('/') }}/edit/admin/company/{{$row->id_user}}" class="text-primary">edit</a>
                                                        &nbsp;&nbsp;
                                                        <a rel="tooltip"  data-toggle="modal" data-target="#popUp1"  href="{{ URL::to('/') }}/ajax/admin/company/{{$row->id_user}}" class="text-danger">remove</a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            <div class="modal fade" id="popUp1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/css/jquery.dataTables.min.css') }}">
@stop

@section('javascript')
<script type="text/javascript" language="javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>

<script type="text/javascript">
    $("#popUp1").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    $(document).ready(function() {
        var myTableRow =  $('#myTable').DataTable( {
                columnDefs: [{
                    orderable: false,
                    className: 'select-checkbox',
                    targets: 0
                }],
                dom: 'Bfrtip',
                "deferRender": true,
                columnDefs: [{ 
                    orderable: false, 
                    targets: 0,
                }],
                searching: true,
                 pagingType: "full_numbers",
                 "paging":   true,
                 "info":     false,
                 "ordering": true,
                 "pageLength": 20
        } );
        
    } );
</script>
@stop
