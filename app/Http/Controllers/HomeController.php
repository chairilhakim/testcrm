<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

use App\Models\Company;
use App\Models\User;

class HomeController extends Controller {
    
    public function __construct(){
        
    }
    
//    public function getHomepage(){
//        return abort(404);
//    }
    
    public function getFront(){
        return redirect()->route('memberLogin');
    }
    
    public function getAdminAreaLogin(){
        return view('frontend.login');
    }
    
    public function postAreaLogin(Request $request){
        $user_code = $request->admin_username;
        $password = $request->admin_password;
        $userdata = array('user_code' => $user_code, 'password'  => $password, 'is_active' => 1);
        if(Auth::guard()->attempt($userdata)){
            $request->session()->regenerate();
            return redirect()->route('admDashboard');
        }
        return redirect()->route('memberLogin')
                ->with('message', 'Incorrect username or password')
                ->with('messageclass', 'danger');
    }
    
    public function getDashboard(){
        $dataUser = Auth::user();
        $modelCompany = New Company;
        $modelUsers = New User;
        $totalCompany = 0;
        $totalEmployee = 0;
        $detailCompany = null;
        if($dataUser->user_type == 1){
            $totalCompany = $modelCompany->getTotalCompany();
            $totalEmployee = $modelUsers->getTotalEmployeeBySuperadmin();
        }
        if($dataUser->user_type == 5){
            $totalEmployee = $modelUsers->getTotalEmployeeByAdmin($dataUser->company_id);
            $detailCompany = $modelCompany->getDetailCompany($dataUser->company_id);
        }
        if($dataUser->user_type == 10){
            $detailCompany = $modelCompany->getDetailCompany($dataUser->company_id);
        }
        return view('admin.dashboard.dashboard')
                    ->with('totalCompany', $totalCompany)
                    ->with('totalEmployee', $totalEmployee)
                    ->with('detailCompany', $detailCompany)
                    ->with('dataUser', $dataUser);
    }
    
    public function getAdminLogout(Request $request) {
        $dataUser = Auth::user();
        Auth::guard()->logout();
        $request->session()->invalidate();
        return redirect()->route('memberLogin');
    }
    
    public function getStorageImage($name){
        $path = storage_path('app/public/' . $name);
        if (!File::exists($path)) {
            abort(404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }
    
}