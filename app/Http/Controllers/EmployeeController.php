<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Models\Company;
use App\Models\User;
use File;
use Validator;


class EmployeeController extends Controller {
    
    public function __construct(){
        
    }
    
    public function getAllEmployee(){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        $modelUser = New User;
        $getData = $modelUser->getAllAdminUser($dataUser->company_id);
        return view('admin.employee.list-employee')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function getAddEmployee(){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        return view('admin.employee.add-employee')
                ->with('dataUser', $dataUser);
    }
    
    public function postAddEmployee(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        $modelUser = New User;
        $modelCompany = New Company;
        //validasi here
        $validator = Validator::make(
            $request->all(),
            [
                "f_name" => "required|max:30",
                "l_name" => "required|max:30",
                "user_code" => "required|max:30|unique:users,user_code",
                "password" => "required|max:50",
                "email" => "required|max:50",
                "phone" => "required|max:15",
            ],
            [
                "f_name.required" => "First Name : Required",
                "f_name.max" => "First Name : Max 30 Characters",

                "l_name.required" => "Last Name : Required",
                "l_name.max" => "Last Name : Max 30 Characters",

                "user_code.required" => "Username : Required",
                "user_code.max" => "Username : Max 30 Characters",
                "user_code.unique" => "Username : Duplicate / Not Available",

                "password.required" => "Password : Required",
                "password.max" => "Password : Max 20 Characters",

                "phone.required" => "Phone Number : Required",
                "phone.max" => "Phone Number : Max 15 Characters",

                "email.required" => "Email : Required",
                "email.max" => "Email : Max 50 Characters",
            ]
        );

        if ($validator->fails()) {
//            dd($validator->messages()->all()[0]);
            return redirect()->route('addEmployee')
                        ->with('message', $validator->messages()->all()[0])
                        ->with('messageclass', 'danger');
        }
        $dataInsert = array(
            'f_name' => $request->f_name,
            'l_name' => $request->l_name,
            'email' => $request->email,
            'phone' => $request->phone,
            'user_code' => $request->user_code,
            'password' => bcrypt($request->password),
            'user_type' => 10,
            'company_id' => $dataUser->company_id
        );
        $modelUser->getInsertUsers($dataInsert);
        $dataCompany = $modelCompany->getDetailCompany($dataUser->company_id);
        $dataEmail = array(
            'name' => $request->f_name.' '.$request->l_name,
            'logo' => $dataCompany->logo
        );
        $emailSend = $request->email;
        Mail::send('admin.employee.email', $dataEmail, function($message) use($emailSend){
            $message->to($emailSend, 'Welcome')
                    ->from('noreply@member.billtrade.us', 'MAILFROM.US')
                    ->subject('Welcome to CRM.');
        });
        return redirect()->route('allEmployee')
                        ->with('message', 'New Employee Success')
                        ->with('messageclass', 'success');
    }
    
    public function getEditEmployee($id){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        $modelCompany = New Company;
        $modelUser = New User;
        $getData = $modelUser->getDetailEmployee($id);
        return view('admin.employee.edit-employee')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function postEditEmployee(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        $modelCompany = New Company;
        $modelUser = New User;
        $getData = $modelUser->getDetailEmployee($request->id);
        $f_name = $getData->f_name;
        if($request->f_name != null){
            if($getData->f_name != $request->f_name){
                $f_name = $request->f_name;
            }
        }
        $l_name = $getData->l_name;
        if($request->l_name != null){
            if($getData->l_name != $request->l_name){
                $l_name = $request->l_name;
            }
        }
        $email = $getData->email;
        if($request->email != null){
            if($getData->email != $request->email){
                $email = $request->email;
            }
        }
        $phone = $getData->phone;
        if($request->phone != null){
            if($getData->phone != $request->phone){
                $phone = $request->phone;
            }
        }
        $user_code = $getData->user_code;
        if($request->user_code != null){
            if($getData->user_code != $request->user_code){
                $user_code = $request->user_code;
            }
        }
        if($request->password != null){
            $dataUpdate = array(
                'f_name' => $f_name,
                'l_name' => $l_name,
                'email' => $email,
                'phone' => $phone,
                'user_code' => $user_code,
                'password' => bcrypt($request->password),
            );
            $modelUser->getUpdateUsers($request->id, $dataUpdate);
        }
        if($request->password == null){
            $dataUpdate = array(
                'f_name' => $f_name,
                'l_name' => $l_name,
                'email' => $email,
                'phone' => $phone,
                'user_code' => $user_code,
            );
            $modelUser->getUpdateUsers($request->id, $dataUpdate);
        }
        return redirect()->route('allEmployee')
                        ->with('message', 'Update Employee Success')
                        ->with('messageclass', 'success');
    }
    
    public function getCekEmployeeById($id){
        $dataUser = Auth::user();
        $modelCompany = New Company;
        $modelUsers = New User;
        $getUser = $modelUsers->getDetailEmployee($id);
        return view('admin.employee.ajax.rm-employee')
                ->with('getData', $getUser)
                ->with('dataUser', $dataUser);
    }
    
    public function postRemoveEmployee(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(5);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        $modelUsers = New User;
        $dataUpdate = array(
            'is_active' => 0,
            'deleted_at' => date('Y-m-d H:i:s')
        );
        $modelUsers->getUpdateUsers($request->cekId, $dataUpdate);
        return redirect()->route('allEmployee')
                        ->with('message', 'Remove Employee Done')
                        ->with('messageclass', 'success');
    }
    
    
    
}