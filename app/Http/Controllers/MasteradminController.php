<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Models\Company;
use App\Models\User;
use File;


class MasteradminController extends Controller {
    
    public function __construct(){
        
    }
    
    public function getAllCompany(){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        $modelCompany = New Company;
        $getData = $modelCompany->getAllCompany();
        return view('admin.company.list-company')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function getAddCompany(){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        return view('admin.company.add-company')
                ->with('dataUser', $dataUser);
    }
    
    public function postAddCompany(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        $modelCompany = New Company;
        $request->validate([
            'file_upload' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
    
        $imageName = time().'.'.$request->file_upload->extension();  
        $request->file_upload->move(storage_path('app/public/'), $imageName);
        $dataInsert = array(
            'name' => $request->name,
            'email' => $request->email,
            'code' => $request->code,
            'website' => $request->website,
            'logo' => $imageName
        );
        $modelCompany->getInsertCompany($dataInsert);
        return redirect()->route('allCompany')
                        ->with('message', 'New Company Success')
                        ->with('messageclass', 'success');
    }
    
    public function getEditCompany($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        $modelCompany = New Company;
        $getData = $modelCompany->getDetailCompany($id);
        return view('admin.company.edit-company')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function postEditCompany(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        $modelCompany = New Company;
        $getData = $modelCompany->getDetailCompany($request->id);
        $name = $getData->name;
        if($request->name != null){
            if($getData->name != $request->name){
                $name = $request->name;
            }
        }
        $email = $getData->email;
        if($request->email != null){
            if($getData->email != $request->email){
                $email = $request->email;
            }
        }
        $code = $getData->code;
        if($request->code != null){
            if($getData->code != $request->code){
                $code = $request->code;
            }
        }
        $website = $getData->website;
        if($request->website != null){
            if($getData->website != $request->website){
                $website = $request->website;
            }
        }
        if($request->file_upload != null){
            $request->validate([
                'file_upload' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $imageName = time().'.'.$request->file_upload->extension();  
            $request->file_upload->move(storage_path('app/public/'), $imageName);
            $dataUpdate = array(
                'name' => $name,
                'email' => $email,
                'code' => $code,
                'website' => $website,
                'logo' => $imageName
            );
            $modelCompany->getUpdateCompany($request->id, $dataUpdate);
        }
        if($request->file_upload == null){
            $dataUpdate = array(
                'name' => $name,
                'email' => $email,
                'code' => $code,
                'website' => $website,
            );
            $modelCompany->getUpdateCompany($request->id, $dataUpdate);
        }
        return redirect()->route('allCompany')
                        ->with('message', 'Update Company Success')
                        ->with('messageclass', 'success');
    }
    
    public function getAllAdminCompany(){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        $modelCompany = New Company;
        $getAllAdmin = $modelCompany->getAllAdminCompany();
        return view('admin.company.list-admincompany')
                ->with('getData', $getAllAdmin)
                ->with('dataUser', $dataUser);
    }
    
    public function getAssignAdminCompany(){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        $modelCompany = New Company;
        $getAllCompany = $modelCompany->getAllCompany();
        if($getAllCompany == null){
            return redirect()->route('addCompany')
                        ->with('message', 'Create Company First')
                        ->with('messageclass', 'danger');
        }
        return view('admin.company.add-admincompany')
                ->with('getAllCompany', $getAllCompany)
                ->with('dataUser', $dataUser);
    }
    
    public function postAssignAdminCompany(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        $modelCompany = New Company;
        $modelUsers = New User;
        $dataInsert = array(
            'f_name' => $request->f_name,
            'l_name' => $request->l_name,
            'email' => $request->email,
            'phone' => $request->phone,
            'company_id' => $request->company_id,
            'user_code' => $request->user_code,
            'password' => bcrypt($request->password),
            'user_type' => 5
        );
        $modelUsers->getInsertUsers($dataInsert);
        return redirect()->route('allAdminCompany')
                        ->with('message', 'Insert Admin Company Success')
                        ->with('messageclass', 'success');
    }
    
    public function getEditAdminCompany($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        $modelCompany = New Company;
        $modelUsers = New User;
        $getUser = $modelUsers->getDetailUser($id);
        $getAllCompany = $modelCompany->getAllCompany();
        return view('admin.company.edit-admincompany')
                ->with('getUser', $getUser)
                ->with('getAllCompany', $getAllCompany)
                ->with('dataUser', $dataUser);
    }
    
    public function postEditAdminCompany(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        $modelCompany = New Company;
        $modelUsers = New User;
        $dataUpdate = array(
            'f_name' => $request->f_name,
            'l_name' => $request->l_name,
            'email' => $request->email,
            'phone' => $request->phone,
            'company_id' => $request->company_id,
            'user_code' => $request->user_code,
        );
        $modelUsers->getUpdateUsers($request->id, $dataUpdate);
        return redirect()->route('allAdminCompany')
                        ->with('message', 'Update Admin Company Success')
                        ->with('messageclass', 'success');
    }
    
    public function postRemoveAdminCompany(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('admDashboard');
        }
        $modelUsers = New User;
        $dataUpdate = array(
            'is_active' => 0,
            'deleted_at' => date('Y-m-d H:i:s')
        );
        $modelUsers->getUpdateUsers($request->cekId, $dataUpdate);
        return redirect()->route('allAdminCompany')
                        ->with('message', 'Remove Admin Company Done')
                        ->with('messageclass', 'success');
    }
    
    //Ajax
    public function getCekAdminCompanyById($id){
        $dataUser = Auth::user();
        $modelCompany = New Company;
        $modelUsers = New User;
        $getUser = $modelUsers->getDetailUser($id);
        return view('admin.company.ajax.rm-admincompany')
                ->with('getData', $getUser)
                ->with('dataUser', $dataUser);
    }
    
}