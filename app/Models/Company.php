<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Company extends Model {
    
    public function getInsertCompany($data){
        try {
            $lastInsertedID = DB::table('company')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdateCompany($id, $data){
        try {
            DB::table('company')->where('id', '=', $id)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getAllCompany(){
        $sql = DB::table('company')->where('is_active', '=', 1)->get();
        $getData = null;
        if(count($sql) > 0){
            $getData = $sql;
        }
        return $getData;
    }
    
    public function getDetailCompany($id){
        $sql = DB::table('company')->where('id', '=', $id)->first();
        return $sql;
    }
    
    public function getAllAdminCompany(){
        $sql = DB::table('company')
                    ->join('users', 'users.company_id', '=', 'company.id')
                    ->selectRaw('company.*,'
                            . 'users.f_name, users.l_name, users.id as id_user')
                    ->where('users.user_type', '=', 5)
                    ->where('users.is_active', '=', 1)
                    ->get();
        return $sql;
    }
    
    public function getTotalCompany(){
        $sql = DB::table('company')
                    ->selectRaw('count(id) as total_company')
                    ->where('is_active', '=', 1)
                    ->first();
        $total_company = 0;
        if($sql->total_company != null){
            $total_company = $sql->total_company;
        }
        return $total_company;
    }
    
    
}