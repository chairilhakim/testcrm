<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable {
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    public function getInsertUsers($data){
        try {
            $lastInsertedID = DB::table('users')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdateUsers($id, $data){
        try {
            DB::table('users')->where('id', '=', $id)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getDetailUser($id){
        $sql = DB::table('users')->where('id', '=', $id)->first();
        return $sql;
    }
    
    public function getDetailEmployee($id){
        $sql = DB::table('users')->where('user_type', '=', 10)->where('id', '=', $id)->first();
        return $sql;
    }
    
    public function getAllAdminUser($company_id){
        $sql = DB::table('users')
                ->where('user_type', '=', 10)
                ->where('company_id', '=', $company_id)
                ->get();
        $getData = null;
        if(count($sql) > 0){
            $getData = $sql;
        }
        return $getData;
    }
    
    public function getTotalEmployeeByAdmin($company_id){
        $sql = DB::table('users')
                    ->selectRaw('count(id) as total_employee')
                    ->where('company_id', '=', $company_id)
                    ->where('user_type', '=', 10)
                    ->first();
        $total_employee = 0;
        if($sql->total_employee != null){
            $total_employee = $sql->total_employee;
        }
        return $total_employee;
    }
    
    public function getTotalEmployeeBySuperadmin(){
        $sql = DB::table('users')
                    ->selectRaw('count(id) as total_employee')
                    ->where('user_type', '=', 10)
                    ->first();
        $total_employee = 0;
        if($sql->total_employee != null){
            $total_employee = $sql->total_employee;
        }
        return $total_employee;
    }
    
    
}
