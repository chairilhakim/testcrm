<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'App\Http\Controllers\HomeController@getFront');
Route::get('/login', 'App\Http\Controllers\HomeController@getAdminAreaLogin')->name('memberLogin');
Route::post('/login_admin', 'App\Http\Controllers\HomeController@postAreaLogin');
Route::get('storage/{name}', 'App\Http\Controllers\HomeController@getStorageImage');


Route::prefix('/')->group(function () {
    Route::get('/dashboard', 'App\Http\Controllers\HomeController@getDashboard')->name('admDashboard')->middleware('auth');
    Route::get('/logout', 'App\Http\Controllers\HomeController@getAdminLogout')->middleware('auth');
    
    //Superadmin
    Route::get('/list/company', 'App\Http\Controllers\MasteradminController@getAllCompany')->name('allCompany')->middleware('auth');
    Route::get('/add/company', 'App\Http\Controllers\MasteradminController@getAddCompany')->name('addCompany')->middleware('auth');
    Route::post('/add/company', 'App\Http\Controllers\MasteradminController@postAddCompany')->middleware('auth');
    Route::get('/edit/company/{id}', 'App\Http\Controllers\MasteradminController@getEditCompany')->name('editCompany')->middleware('auth');
    Route::post('/edit/company', 'App\Http\Controllers\MasteradminController@postEditCompany')->middleware('auth');
    
    Route::get('/list/admin/company', 'App\Http\Controllers\MasteradminController@getAllAdminCompany')->name('allAdminCompany')->middleware('auth');
    Route::get('/assign/admin/company', 'App\Http\Controllers\MasteradminController@getAssignAdminCompany')->name('assignAdminCompany')->middleware('auth');
    Route::post('/assign/admin/company', 'App\Http\Controllers\MasteradminController@postAssignAdminCompany')->middleware('auth');
    Route::get('/edit/admin/company/{id}', 'App\Http\Controllers\MasteradminController@getEditAdminCompany')->name('editAdminCompany')->middleware('auth');
    Route::post('/edit/admin/company', 'App\Http\Controllers\MasteradminController@postEditAdminCompany')->middleware('auth');
    Route::post('/rm/admin/company', 'App\Http\Controllers\MasteradminController@postRemoveAdminCompany')->middleware('auth');
    
    //Admin Company
    Route::get('/list/employee', 'App\Http\Controllers\EmployeeController@getAllEmployee')->name('allEmployee')->middleware('auth');
    Route::get('/add/employee', 'App\Http\Controllers\EmployeeController@getAddEmployee')->name('addEmployee')->middleware('auth');
    Route::post('/add/employee', 'App\Http\Controllers\EmployeeController@postAddEmployee')->middleware('auth');
    Route::get('/edit/employee/{id}', 'App\Http\Controllers\EmployeeController@getEditEmployee')->name('editEmployee')->middleware('auth');
    Route::post('/edit/employee', 'App\Http\Controllers\EmployeeController@postEditEmployee')->middleware('auth');
    Route::post('/rm/employee', 'App\Http\Controllers\EmployeeController@postRemoveEmployee')->middleware('auth');
    
    //Employee
    
    //Ajax
    Route::get('/ajax/admin/company/{id}', 'App\Http\Controllers\MasteradminController@getCekAdminCompanyById')->middleware('auth');
    Route::get('/ajax/employee/{id}', 'App\Http\Controllers\EmployeeController@getCekEmployeeById')->middleware('auth');
});
//Route::get('/', function () {
//    return view('welcome');
//});
